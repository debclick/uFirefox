#!/bin/sh
pkg="$1"
mkdir -p ${CLICK_LD_LIBRARY_PATH} ${CLICK_PATH} ${INSTALL_DIR} ${BUILD_DIR}/archives/partial
cp ./adapt.sh ./build.sh ./apparmor.json ./manifest.json ${BUILD_DIR}
cp ./adapt.sh ./build.sh ./syspref.js ./apparmor.json ./manifest.json ${BUILD_DIR}
cp ./${pkg}.desktop ${BUILD_DIR}/*.json ${INSTALL_DIR}
cp ./${pkg}.sh ${CLICK_PATH}
