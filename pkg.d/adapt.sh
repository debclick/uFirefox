sed -i "/^Terminal=/i Icon=share/icons/hicolor/symbolic/apps/firefox-symbolic.svg" ${INSTALL_DIR}/firefox.desktop
#a short description for the package
sed -i "s|@description@|firefox is a webbrowser|" ${INSTALL_DIR}/manifest.json

#adapt to fit the needs of the package
rm ${CLICK_PATH}/firefox
cd ${CLICK_PATH}
ln -s ../firefox/firefox .
rm ${CLICK_LD_LIBRARY_PATH}/firefox/browser/defaults/preferences/syspref.js
cp ${BUILD_DIR}/syspref.js ${CLICK_LD_LIBRARY_PATH}/firefox/browser/defaults/preferences/syspref.js
cp ${BUILD_DIR}/immodules.cache ${CLICK_LD_LIBRARY_PATH}/gtk-3.0/3.0.0/immodules/
sed -i "s/@CLICK_ARCH@/${ARCH_TRIPLET}/" ${CLICK_PATH}/firefox.sh
sed -i "s/@CLICK_ARCH@/${ARCH_TRIPLET}/" ${CLICK_LD_LIBRARY_PATH}/gtk-3.0/3.0.0/immodules/immodules.cache
